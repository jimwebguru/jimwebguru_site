/*
 * Utils.java
 *
 * Created on October 1, 2006, 3:18 AM
 *
 */

package me.jamesmay;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.servlet.http.HttpServletRequest;
import javax.faces.context.FacesContext;
import javax.faces.component.UIComponent;
import javax.servlet.http.HttpServletResponse;

public class Utils
{
	private static final String BUNDLE_FILE = "com.bizznetworx.Bundle";
	public static final String BUNDLE_VAR_NAME = "bundle";
	private static String datePattern = "MM/dd/yyyy";
	private static SimpleDateFormat dFormatter = new SimpleDateFormat(datePattern);

	/** Creates a new instance of Utils */
	public Utils()
	{

	}

	public static ResourceBundle getResourceBundle()
	{
		return ResourceBundle.getBundle(BUNDLE_FILE);
	}

	public static HttpServletRequest getRequest()
	{
		FacesContext context = FacesContext.getCurrentInstance();

		if(context != null)
		{
			return (HttpServletRequest)context.getExternalContext().getRequest();
		}

		return null;
	}

	public static HttpServletResponse getResponse()
	{
		FacesContext context = FacesContext.getCurrentInstance();

		if(context != null)
		{
			return (HttpServletResponse)context.getExternalContext().getResponse();
		}

		return null;
	}

	public static UIComponent getComponentById(String id)
	{
		return FacesContext.getCurrentInstance().getViewRoot().findComponent(id);
	}

	public static Object getManagedBean(String beanName)
	{
		FacesContext context = FacesContext.getCurrentInstance();

		return context.getApplication().getELResolver().getValue(context.getELContext(),null,beanName);
	}

	public static ValueExpression getExpression(String ex, Class c)
	{
		FacesContext context = FacesContext.getCurrentInstance();
		ExpressionFactory factory = context.getApplication().getExpressionFactory();
		ValueExpression expression = factory.createValueExpression(context.getELContext(),ex,c);

		return expression;
	}

	public static ELContext getELContext()
	{
		FacesContext context = FacesContext.getCurrentInstance();
		return context.getELContext();
	}

	public static Object getValueOfExpression(String ex, Class c)
	{
		FacesContext context = FacesContext.getCurrentInstance();
		ExpressionFactory factory = context.getApplication().getExpressionFactory();
		ValueExpression expression = factory.createValueExpression(context.getELContext(),ex,c);

		return expression.getValue(context.getELContext());
	}

	public static void setValueOfExpression(String ex,Class c,Object value)
	{
		FacesContext context = FacesContext.getCurrentInstance();
		ExpressionFactory factory = context.getApplication().getExpressionFactory();
		ValueExpression expression = factory.createValueExpression(context.getELContext(),ex,c);

		expression.setValue(context.getELContext(),value);
	}

	public static void setRequestAttributes(HttpServletRequest request)
	{
		Map pMap = request.getParameterMap();

		for(Object key : pMap.keySet())
		{
			if(request.getAttribute(key.toString()) == null)
			{
				request.setAttribute(key.toString(),pMap.get(key));
			}
		}
	}

	public static void logError(Exception ex)
	{
		Logger.getLogger(Utils.class.getName()).log(Level.SEVERE,"exception caught" ,ex);
	}

	public static void addMessage(String message,Severity severity)
	{
		FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(severity,message,message));
	}

	public static String formatDate(Date date)
	{
		try
		{
			return dFormatter.format(date);
		}
		catch(Exception ex)
		{

		}

		return "";
	}

	public static String getDatePattern()
	{
		return datePattern;
	}

	public static Date toDate(String dateValue)
	{
		try
		{
			return dFormatter.parse(dateValue);
		}
		catch(Exception ex)
		{

		}

		return null;
	}

	public static Date toDate(Date date)
	{
		try
		{
			GregorianCalendar cal = new GregorianCalendar();
			cal.setTime(date);

			return dFormatter.parse(cal.get(Calendar.MONTH) + "/" + cal.get(Calendar.DAY_OF_MONTH) + "/" + cal.get(Calendar.YEAR));
		}
		catch(Exception ex)
		{

		}

		return null;
	}

	public static String getQueryStringValue(String indexName)
	{
		HttpServletRequest request = Utils.getRequest();
		String queryString = request.getQueryString();
		String queryStringValue = null;

		if(queryString != null)
		{
			String[] queryArray = queryString.split("&");

			for(String queryPart : queryArray)
			{
				String[] parameterItem = queryPart.split("=");

				if(parameterItem[0].equals(indexName))
				{
					queryStringValue = parameterItem[1];
				}
			}
		}

		return queryStringValue;
	}

	public static String getSearchString(int type)
	{
		String returnValue = null;

		String searchCategory = Utils.getQueryStringValue("searchCategory");

		String[] searchCategoryArray = null;

		if(searchCategory != null)
		{
			searchCategoryArray = searchCategory.split("%7C");
		}

		switch(type)
		{
			case 0:     returnValue = searchCategoryArray[0];
				break;

			case 1:     returnValue = searchCategoryArray[1];
				break;

			case 2:     returnValue = searchCategoryArray[2];
				break;
		}

		return returnValue;
	}
}

