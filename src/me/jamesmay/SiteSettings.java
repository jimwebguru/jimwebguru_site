/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package me.jamesmay;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

/**
 *
 * @author bizznetworx
 */
@Named
@ApplicationScoped
public class SiteSettings
{
	private String siteUrl;

	private String siteFromEmailAddress;

	private String contactUsEmailAddress;

	private int siteId;

	/** Creates a new instance of SiteSettings */
	public SiteSettings()
	{
		this.siteUrl = "http://www.jamesmay.me/";
		this.siteFromEmailAddress = "websites@bizznetworxonline.com";
		this.contactUsEmailAddress = "jim.webguru@gmail.com";
		this.siteId = 6;
	}

	public String getSiteUrl()
	{
		return siteUrl;
	}

	public void setSiteUrl(String siteUrl)
	{
		this.siteUrl = siteUrl;
	}

	public String getContactUsEmailAddress()
	{
		return contactUsEmailAddress;
	}

	public void setContactUsEmailAddress(String contactUsEmailAddress)
	{
		this.contactUsEmailAddress = contactUsEmailAddress;
	}

	public String getSiteFromEmailAddress()
	{
		return siteFromEmailAddress;
	}

	public void setSiteFromEmailAddress(String siteFromEmailAddress)
	{
		this.siteFromEmailAddress = siteFromEmailAddress;
	}

	public int getSiteId()
	{
		return siteId;
	}

	public void setSiteId(int siteId)
	{
		this.siteId = siteId;
	}
}

