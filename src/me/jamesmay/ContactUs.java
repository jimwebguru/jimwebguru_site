/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package me.jamesmay;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Named;
/**
 *
 * @author bizznetworx
 */
@Named
@ViewScoped
public class ContactUs implements Serializable
{
	private String emailAddress;

	private String description;

	private String botTrap;

	private boolean emailSent = false;

	/** Creates a new instance of ContactUs */
	public ContactUs()
	{
	}

	public String getEmailAddress()
	{
		return emailAddress;
	}

	public void setEmailAddress(String email)
	{
		this.emailAddress = email;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getBotTrap()
	{
		return botTrap;
	}

	public void setBotTrap(String botTrap)
	{
		this.botTrap = botTrap;
	}

	public boolean isEmailSent()
	{
		return emailSent;
	}

	public void setEmailSent(boolean emailSent)
	{
		this.emailSent = emailSent;
	}

	public void sendEmail()
	{
		if(this.botTrap == null || this.botTrap.trim().isEmpty())
		{
			StringBuilder emailBuffer = new StringBuilder();
			emailBuffer.append("<email>");
			emailBuffer.append("<emailAddress>");
			emailBuffer.append(this.emailAddress);
			emailBuffer.append("</emailAddress>");
			emailBuffer.append("<description>");
			emailBuffer.append(this.description);
			emailBuffer.append("</description>");
			emailBuffer.append("</email>");

			me.jamesmay.SiteSettings siteSettings = (me.jamesmay.SiteSettings) me.jamesmay.Utils.getManagedBean("siteSettings");

			me.jamesmay.EmailUtil.sendEmail(siteSettings.getContactUsEmailAddress(), siteSettings.getSiteFromEmailAddress(), "JamesMay.me Contact Form", emailBuffer.toString(), me.jamesmay.EmailUtil.CONTACTUS_XSLT);

			this.emailSent = true;
		}
	}
}
