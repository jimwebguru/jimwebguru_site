<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : ContactUsEmail.xsl
    Created on : November 5, 2010, 1:10 PM
    Author     : jmay
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <xsl:template match="/">
        <html>
            <head>
                <title>James May Contact</title>
            </head>
            <body>
                User with email <xsl:value-of select="email/emailAddress" /> has requested to be contacted.
                <br /><br />
                Email Message from the user:
                <br/><br/>
                <xsl:value-of select="email/description" />
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
